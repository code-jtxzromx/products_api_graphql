<?php

namespace App\Libraries;

class libAlgorithms
{
    public static function multipleCartesian($list)
    {
        $temp = array_shift($list);

        foreach ($list as $item) {
            $temp = self::cartesianProduct($temp, $item);
        }

        return $temp;
    }

    public static function cartesianProduct($setA, $setB)
    {
        $result = [];

        foreach ($setA as $itemA) {
            foreach ($setB as $itemB) {
                $temp = [];
                if (is_array($itemA)) {
                    $temp = $itemA;
                } else {
                    array_push($temp, $itemA);
                }
                array_push($temp, $itemB);
                array_push($result, $temp);
            }
        }

        return $result;
    }
}
