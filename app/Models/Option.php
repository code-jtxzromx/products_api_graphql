<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $table = 't_option';

    protected $primaryKey = 'option_id';

    protected $fillable = ['product_no', 'option_type', 'option_name'];

    public function option_values()
    {
        return $this->hasMany(OptionValue::class, 'option_id');
    }
}
