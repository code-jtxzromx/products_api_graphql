<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VariantOption extends Model
{
    protected $table = 't_variant_option';

    protected $fillable = ['variant_id', 'option_value_id'];

    public function option_value()
    {
        return $this->belongsTo(OptionValue::class, 'option_value_id');
    }
}
