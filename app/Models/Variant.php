<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Variant extends Model
{
    protected $table = 't_variant';

    protected $primaryKey = 'variant_id';

    protected $fillable = ['variant_code', 'display', 'selling', 'product_no'];

    public function variant_options()
    {
        return $this->hasMany(VariantOption::class, 'variant_id');
    }
}
