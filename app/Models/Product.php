<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 't_product';

    protected $primaryKey = 'product_no';

    protected $fillable = [
        'category_no', 'product_name', 'product_code', 'model_name', 'price', 'display', 'selling', 'product_condition',
        'summary_description', 'has_option', 'option_type', 'manufacturer', 'brand', 'supplier', 'additional_price', 'discount_price'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_no');
    }

    public function images()
    {
        return $this->hasMany(Image::class, 'product_no');
    }

    public function options()
    {
        return $this->hasMany(Option::class, 'product_no');
    }

    public function variants()
    {
        return $this->hasMany(Variant::class, 'product_no');
    }
}
