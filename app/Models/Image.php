<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 't_image';

    protected $fillable = ['product_no', 'detail_image'];
}
