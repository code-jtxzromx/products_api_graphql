<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 't_category';

    protected $primaryKey = 'category_no';

    protected $fillable = [
        'category_name', 'display_type'
    ];

    public function products()
    {
        return $this->hasMany(Product::class, 'product_no');
    }
}
