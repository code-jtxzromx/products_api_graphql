<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OptionValue extends Model
{
    protected $table = 't_option_value';

    protected $primaryKey = 'option_value_id';

    protected $fillable = ['option_id', 'option_text'];
}
