<?php

namespace App\GraphQL\Mutations;

use App\Models\Product;
use Illuminate\Support\Arr;

class ProductMutator
{
    public function create($root, array $args)
    {
        return Product::create($args);
    }

    public function update($root, array $args)
    {
        $aProduct = Product::find($args['product_no']);
        $aProduct->update(Arr::except($args, ['product_no']));
        return Product::find($args['product_no']);
    }

    public function delete($root, array $args)
    {
        $aProduct = Product::find($args['product_no']);
        $aProduct->is_deleted = true;
        $aProduct->save();
        return Product::find($args['product_no']);
    }
}
