<?php

namespace App\GraphQL\Mutations;

use App\Libraries\libAlgorithms;
use App\Models\Option;
use App\Models\OptionValue;
use App\Models\Product;
use App\Models\Variant;
use App\Models\VariantOption;

class OptionMutator
{
    public function create($root, array $args)
    {
        $product = Product::where('product_no', $args['product_no'])->first();

        foreach ($args['options'] as $option) {
            $aOption = Option::create([
                'product_no' => $args['product_no'],
                'option_type' => $option['option_type'],
                'option_name' => $option['option_name']
            ]);

            foreach ($option['option_values'] as $option_value) {
                OptionValue::create([
                    'option_id' => $aOption->option_id,
                    'option_text' => $option_value
                ]);
            }
        }

        $aOptionValues_Plain = [];
        $aOptionValues_Array = [];
        $aOptions = Option::where('product_no', $args['product_no'])->get();
        foreach ($aOptions as $option) {
            array_push($aOptionValues_Array, OptionValue::where('option_id', $option->option_id)->get()->toArray());
            array_push($aOptionValues_Plain, OptionValue::where('option_id', $option->option_id)->pluck('option_text')->toArray());
        }

        $aVariants = libAlgorithms::multipleCartesian($aOptionValues_Plain);

        foreach ($aVariants as $key => $aVariant) {
            $variant = Variant::create([
                'variant_code' =>  $product->product_code . sprintf("%04d", $key),
                'display' => true,
                'selling' => true,
                'product_no' => $product->product_no
            ]);

            foreach ($aVariant as $index => $sOption) {
                $iKey = array_search($sOption, array_column($aOptionValues_Array[$index], 'option_text'));

                VariantOption::create([
                    'variant_id' => $variant->variant_id,
                    'option_value_id' => $aOptionValues_Array[$index][$iKey]['option_value_id']
                ]);
            }
        }

        return Option::where('product_no', $args['product_no'])->get();
    }

    public function delete($root, array $args)
    {
        $aVariants = Variant::where('product_no', $args['product_no'])->pluck('variant_id')->toArray();
        $aOptions = Option::where('product_no', $args['product_no'])->pluck('option_id')->toArray();

        VariantOption::whereIn('variant_id', $aVariants)->delete();
        OptionValue::whereIn('option_id', $aOptions)->delete();
        Variant::whereIn('variant_id', $aVariants)->delete();
        Option::whereIn('option_id', $aOptions)->delete();

        return Option::where('product_no', $args['product_no'])->get();
    }
}
