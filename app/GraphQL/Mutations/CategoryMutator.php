<?php

namespace App\GraphQL\Mutations;

use App\Models\Category;
use Illuminate\Support\Arr;

class CategoryMutator
{
    public function create($root, array $args)
    {
        return Category::create($args);
    }

    public function update($root, array $args)
    {
        $aCategory = Category::find($args['category_no']);
        $aCategory->update(Arr::except($args, ['category_no']));
        return Category::find($args['category_no']);
    }

    public function delete($root, array $args)
    {
        $aCategory = Category::find($args['category_no']);
        $aCategory->is_deleted = true;
        $aCategory->save();
        return Category::find($args['category_no']);
    }
}
