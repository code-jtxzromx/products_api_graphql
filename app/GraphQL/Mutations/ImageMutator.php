<?php

namespace App\GraphQL\Mutations;

use App\Models\Image;
use Illuminate\Support\Arr;

class ImageMutator
{
    public function create($root, array $args)
    {
        return Image::create($args);
    }

    public function update($root, array $args)
    {
        $aImage = Image::find($args['id']);
        $aImage->update(Arr::except($args, ['id']));
        return Image::find($args['id']);
    }

    public function delete($root, array $args)
    {
        $aImage = Image::find($args['id']);
        $aImage->delete();
        return Image::find($args['id']);
    }
}
