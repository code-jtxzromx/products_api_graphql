### Sample Queries

- Fetch All Products
```graphql
query {
    products(first: 50 page: 1) {
        data {
            product_no
            category {
                category_name
                display_type
            }
            product_code
            product_name
            model_name
            price
            display
            selling
            product_condition
            summary_description
            has_option
            option_type
            manufacturer
            brand
            supplier
            additional_price
            discount_price
            images {
                detail_image
            }
            options {
                option_type
                option_name
                option_values {
                    option_text
                }
            }
            is_deleted
        }
    }
}
```

- Get Product
```graphql
query {
    product(product_no: 1) {
        product_no
        category {
            category_name
            display_type
        }
        product_code
        product_name
        model_name
        price
        display
        selling
        product_condition
        summary_description
        has_option
        option_type
        manufacturer
        brand
        supplier
        additional_price
        discount_price
        images {
            detail_image
        }
        options {
            option_type
            option_name
            option_values {
                option_text
            }
        }
        is_deleted
    }
}
```

- Fetch All Categories
```graphql
query {
    categories (first: 10) {
        paginatorInfo {
            currentPage lastPage hasMorePages total
        }
        data {
            category_no
            category_name
            display_type
            is_deleted
        }
    }
}
```

- Get Category
```graphql
query {
    category (category_no: 1) {
        category_no
        category_name
        display_type
        is_deleted
    }
}
```

- Add Category
```graphql
mutation {
    createCategory (
        category_name: "Consoles"
        display_type: "A"
    ) {
        category_no
        category_name
        display_type
    }
}
```

- Edit Category
```graphql
mutation {
    updateCategory (
        category_no: 6
        category_name: "Gaming Consoles"
    ) {
        category_no
        category_name
        display_type
    }
}
```

- Delete Category
```graphql
mutation {
    deleteCategory (
        category_no: 6
    ) {
        category_no
        category_name
        display_type
        is_deleted
    }
}
```

- Add Product
```graphql
mutation {
    createProduct(
        category_no: 4,
        product_code: "P0004003"
        product_name: "Sample Product A"
        model_name: "GQLP123"
        price: 60500.00
    ) {
        product_no
        product_code
        product_name
        model_name
        price
        display
        selling
        product_condition
        summary_description
        has_option
        option_type
        manufacturer
        brand
        supplier
        additional_price
        discount_price
        category {
            category_name
            display_type
        }
    }
}
```

- Edit Product
```graphql
mutation {
    updateProduct(
        product_no: 11,
        category_no: 4,
        product_name: "Sample Product A"
        model_name: "GQLP123"
        price: 60500.00
    ) {
        product_no
        product_code
        product_name
        model_name
        price
        display
        selling
        product_condition
        summary_description
        has_option
        option_type
        manufacturer
        brand
        supplier
        additional_price
        discount_price
        category {
            category_name
            display_type
        }
    }
}
```

- Delete Product
```graphql
mutation {
    deleteProduct(
        product_no: 11,
    ) {
        product_no
        product_code
        product_name
        is_deleted
    }
}
```

- Add Image
```graphql
mutation {
    createImage(
        product_no: 1,
        detail_image: "https://loremflickr.com/320/240"
    ) {
        detail_image
    }
}
```

- Edit Image
```graphql
mutation {
    updateImage(
        id: 11
        product_no: 1,
        detail_image: "https://loremflickr.com/320/240"
    ) {
        detail_image
    }
}
```

- Delete Image
```graphql
mutation {
    deleteImage(
        id: 1,
    ) {
        detail_image
    }
}
```

- Add Option
```graphql
mutation {
    createOption(
        product_no: 12,
        options: [
            {
                option_name: "Storage",
                option_values: [
                    "64 GB",
                    "128 GB",
                    "256 GB"
                ]
            },
            {
                option_name: "Memory",
                option_values: [
                    "6 GB",
                    "8 GB"
                ]
            },
            {
                option_name: "Color",
                option_values: [
                    "Black",
                    "White",
                ]
            },
        ]
    ) {
        option_id
        option_type
        option_name
        option_values {
            option_text
        }
    }
}
```

- Delete Option
```graphql
mutation {
    deleteOption(
        product_no: 12
    ) {
        option_id
    }
}
```
