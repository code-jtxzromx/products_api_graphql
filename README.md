### Introduction
GraphQL Laravel Sample Project 
- Sample Implementation of Lighthouse for Laravel with Sample Database
- Sample Queries: [link](sample%20query.md)
 
### Note
- To run use `php artisan serve`
- Use this settings on `.env`:
```text
DB_CONNECTION=sqlite
# DB_HOST=127.0.0.1
# DB_PORT=3306
# DB_DATABASE=laravel
# DB_USERNAME=root
# DB_PASSWORD=
```

- If you want to use this with MySQL just change the `.env` settings, then run migrations and seeder
```shell script
php artisan serve
php artisan db:seed
```
