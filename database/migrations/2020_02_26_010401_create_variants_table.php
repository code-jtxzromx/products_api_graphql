<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_variant', function (Blueprint $table) {
            $table->bigIncrements('variant_id');
            $table->string('variant_code', 12);
            $table->boolean('display');
            $table->boolean('selling');
            $table->unsignedBigInteger('product_no');
            $table->timestamps();

            $table->foreign('product_no')->references('product_no')->on('t_product');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_variant');
    }
}
