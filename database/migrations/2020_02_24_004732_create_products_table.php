<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_product', function (Blueprint $table) {
            $table->bigIncrements('product_no');
            $table->string('product_code', 8);
            $table->string('product_name', 250);
            $table->string('model_name', 100);
            $table->decimal('price', 10, 2);
            $table->boolean('display');
            $table->boolean('selling');
            $table->char('product_condition');
            $table->string('summary_description', 255);
            $table->boolean('has_option');
            $table->char('option_type');
            $table->string('manufacturer',100);
            $table->string('brand', 50);
            $table->string('supplier', 50);
            $table->decimal('additional_price', 10, 2);
            $table->decimal('discount_price', 10, 2);
            $table->unsignedBigInteger('category_no');
            $table->boolean('is_deleted')->default(false);
            $table->timestamps();

            $table->foreign('category_no')->references('category_no')->on('t_category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_product');
    }
}
