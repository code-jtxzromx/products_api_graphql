<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOptionValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_option_value', function (Blueprint $table) {
            $table->bigIncrements('option_value_id');
            $table->unsignedBigInteger('option_id');
            $table->string('option_text', 50);
            $table->timestamps();

            $table->foreign('option_id')->references('option_id')->on('t_option');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_option_value');
    }
}
