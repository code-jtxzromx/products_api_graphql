<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_option', function (Blueprint $table) {
            $table->bigIncrements('option_id');
            $table->unsignedBigInteger('product_no');
            $table->char('option_type');
            $table->string('option_name', 50);
            $table->timestamps();

            $table->foreign('product_no')->references('product_no')->on('t_product');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_option');
    }
}
