<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_image', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('product_no');
            $table->text('detail_image');
            $table->timestamps();

            $table->foreign('product_no')->references('product_no')->on('t_product');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_image');
    }
}
