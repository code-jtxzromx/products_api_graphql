<?php

use Illuminate\Database\Seeder;

class OptionValuesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        \App\Models\OptionValue::truncate();

        \App\Models\Option::all()->each(function ($option) {
            $aOptions = [
                'Storage' => [
                    '64 GB', '128 GB', '256 GB'
                ],
                'Memory' => [
                    '4 GB', '6 GB'
                ],
                'Color' => [
                    'Black', 'White'
                ]
            ];

            foreach ($aOptions[$option->option_name] as $sOption) {
                \App\Models\OptionValue::create([
                    'option_id' => $option->option_id,
                    'option_text' => $sOption
                ]);
            }
        });
        Schema::enableForeignKeyConstraints();
    }
}
