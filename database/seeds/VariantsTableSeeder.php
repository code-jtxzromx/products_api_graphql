<?php

use App\Libraries\libAlgorithms;
use Illuminate\Database\Seeder;

class VariantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        \App\Models\Variant::truncate();
        \App\Models\VariantOption::truncate();

        \App\Models\Product::all()->each(function ($product) {
            $aOptionValues_Plain = [];
            $aOptionValues_Array = [];
            $aOptions = \App\Models\Option::where('product_no', $product->product_no)->get();
            foreach ($aOptions as $option) {
                array_push($aOptionValues_Array, \App\Models\OptionValue::where('option_id', $option->option_id)->get()->toArray());
                array_push($aOptionValues_Plain, \App\Models\OptionValue::where('option_id', $option->option_id)->pluck('option_text')->toArray());
            }

            $aVariants = libAlgorithms::multipleCartesian($aOptionValues_Plain);

            foreach ($aVariants as $key => $aVariant) {
                $variant = \App\Models\Variant::create([
                    'variant_code' =>  $product->product_code . sprintf("%04d", $key),
                    'display' => true,
                    'selling' => true,
                    'product_no' => $product->product_no
                ]);

                foreach ($aVariant as $index => $sOption) {
                    $iKey = array_search($sOption, array_column($aOptionValues_Array[$index], 'option_text'));

                    \App\Models\VariantOption::create([
                        'variant_id' => $variant->variant_id,
                        'option_value_id' => $aOptionValues_Array[$index][$iKey]['option_value_id']
                    ]);
                }
            }
        });

        Schema::enableForeignKeyConstraints();
    }
}
