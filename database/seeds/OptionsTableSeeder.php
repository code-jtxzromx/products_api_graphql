<?php

use Illuminate\Database\Seeder;

class OptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        \App\Models\Option::truncate();

        \App\Models\Product::all()->each(function ($product) {
            $aOptions = [ 'Storage', 'Memory', 'Color' ];

            foreach ($aOptions as $aOption) {
                \App\Models\Option::create([
                    'product_no' => $product->product_no,
                    'option_type' => 'T',
                    'option_name' => $aOption
                ]);
            }
        });
        Schema::enableForeignKeyConstraints();
    }
}
