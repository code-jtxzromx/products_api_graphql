<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        \App\Models\Category::truncate();

        $aCategories = [ 'Mobiles', 'Tablets', 'Camera', 'Audio Devices', 'Desktops' ];

        foreach ($aCategories as $aCategory) {
            \App\Models\Category::create([
                'category_name' => $aCategory,
                'display_type' => 'A'
            ]);
        }
        Schema::enableForeignKeyConstraints();
    }
}
