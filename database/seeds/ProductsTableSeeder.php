<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        \App\Models\Product::truncate();

        $faker = \Faker\factory::create();
        \App\Models\Category::all()->each(function ($category) use ($faker) {
            for($i = 1; $i <= 2; $i++) {
                \App\Models\Product::create([
                    'product_code' => 'P' . sprintf('%03d', $category->category_no) . sprintf('%04d', $i),
                    'product_name' => $faker->sentence(5),
                    'model_name' => 'GQL' . sprintf('%02d', $category->category_no) . sprintf('%02d', $i),
                    'price' => $faker->randomFloat(2,1000, 5000),
                    'display' => true,
                    'selling' => true,
                    'product_condition' => $faker->randomElement(['N', 'B', 'R', 'U', 'E', 'F', 'S']),
                    'summary_description' => $faker->sentence('15'),
                    'has_option' => true,
                    'option_type' => 'C',
                    'manufacturer' => $faker->company,
                    'brand' => $faker->company,
                    'supplier' => $faker->company,
                    'additional_price' =>$faker->randomFloat(2,0, 100),
                    'discount_price' =>$faker->randomFloat(2,0, 100),
                    'category_no' => $category->category_no
                ]);
            }
        });
        Schema::enableForeignKeyConstraints();
    }
}
