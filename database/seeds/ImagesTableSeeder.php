<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class ImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        \App\Models\Image::truncate();

        \App\Models\Product::all()->each(function ($product) {
            \App\Models\Image::create([
                'product_no' => $product->product_no,
                'detail_image' => 'https://loremflickr.com/320/240'
            ]);
        });

        Schema::enableForeignKeyConstraints();
    }
}
